/*

API Gateway resolver

runtime : nodejs8.10

env vars : CONNECTION_STRING

psql user : apicommon

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

switch attached_API_endpoints :
    - GET /users/{u_id}/missions
    - GET /missions/{m_id}

returns client object : 
{
    "id": "clientId",
    "name": "clientName",
    "logo": "clientLogo"
}

*/

'use strict';

const mainModule = require('mainModule');
const pg = require('pg');

function checkSalesMan(field, checker) {
    if (!checker) {
        return null;
    } else {
        return field;
    }
}

// create paginated response (GET /users/{u_id}/missions)
async function createPaginatedResponse(event, query) {
    // check query string parameters (page, search, sort)
    const params = await mainModule.resolveParams(event.queryStringParameters).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: err.callbackStatusCode,
            callbackBody: err.callbackBody
        });
    });
    // query DB
    const result = await mainModule.pgQuery(`${query} ${params.sort} ${params.search} limit ${global.pageSize} offset ${params.page * global.pageSize};`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 404 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }
    // format result to be returned
    let endResult = [];
    for (let i in result.rows) {
        if(result.rows[i].site_sales_man) {
            const trader = await mainModule.pgQuery(`SELECT id,lastname,firstname,email,picture FROM users_get_byid(${result.rows[i].site_sales_man});`).catch((err) => {
                console.error(err);
                return Promise.reject({
                    callbackStatusCode: 500,
                    callbackBody: JSON.stringify('Internal server error')
                });
            });
            
            endResult.push(
                {
                    "client_id": result.rows[i].client_id,
                    "client": result.rows[i].client_name,
                    "logo": result.rows[i].client_logo,
                    "site_id": result.rows[i].site_id,
                    "site": result.rows[i].site_name,
                    "id": result.rows[i].id,
                    "mission": result.rows[i].name,
                    "descr": result.rows[i].description,
                    "date_start": result.rows[i].start_date,
                    "date_end": result.rows[i].end_date,
                    "trader_id": trader.rows[0].id,
                    "trader_familyname": trader.rows[0].lastname,
                    "trader_firstname": trader.rows[0].firstname,
                    "trader_email": trader.rows[0].email,
                    "trader_picture": trader.rows[0].picture
                });
        } else {
            endResult.push(
                {
                    "client_id": result.rows[i].client_id,
                    "client": result.rows[i].client_name,
                    "logo": result.rows[i].client_logo,
                    "site_id": result.rows[i].site_id,
                    "site": result.rows[i].site_name,
                    "id": result.rows[i].id,
                    "mission": result.rows[i].name,
                    "descr": result.rows[i].description,
                    "date_start": result.rows[i].start_date,
                    "date_end": result.rows[i].end_date,
                    "trader_id": null,
                    "trader_familyname": null,
                    "trader_firstname": null,
                    "trader_email": null,
                    "trader_picture": null
                });
        }
            /*{
            "client": {
                "id": result.rows[i].client_id,
                "name": result.rows[i].client_name,
                "logo": result.rows[i].client_logo
            },
            "site": {
                "id": result.rows[i].site_id,
                "name": result.rows[i].site_name
            },
            "id": result.rows[i].id,
            "name": result.rows[i].name,
            "description": result.rows[i].description,
            "start_date": result.rows[i].start_date,
            "end_date": result.rows[i].end_date,
            "user": {
                "id": result.rows[i].user_id,
                "lastname": result.rows[i].user_lastname,
                "firstname": result.rows[i].user_firstname,
                "picture": result.rows[i].user_picture
            }
        }*/
        
    }
    // format REST API response
    const count = await mainModule.pgQuery(`${query.replace('*', 'count(id)')} ${params.search}`);

    const response = {
        "_links": await mainModule.createJsonObject(event.path, params.page, `${query.replace('*', 'count(id)')} ${params.search};`),
        "size": result.rows.length,
        "start": params.page * global.pageSize,
        "count": count.rows[0].count,
        "results": endResult
    };
    // return JSON formatted response
    return JSON.stringify(response);
}

async function createSingleResponse(id, path) {

    const result = await mainModule.pgQuery(`select * from missions_get_byid(${id});`);

    const response = {
        "_links": {
            "self": path
        },
        "results": {
            "client_id": result.rows[0].client_id,
            "client": result.rows[0].client_name,
            "logo": result.rows[0].client_logo,
            "site_id": result.rows[0].site_id,
            "site": result.rows[0].site_name,
            "id": result.rows[0].id,
            "mission": result.rows[0].name,
            "descr": result.rows[0].description,
            "date_start": result.rows[0].start_date,
            "date_end": result.rows[0].end_date
        }

        /*{
            "client": {
                "id": result.rows[0].client_id,
                "name": result.rows[0].client_name,
                "logo": result.rows[0].client_logo
            },
            "site": {
                "id": result.rows[0].site_id,
                "name": result.rows[0].site_name
            },
            "user": {
                "id": result.rows[0].user_id,
                "lastname": result.rows[0].user_lastname,
                "firstname": result.rows[0].user_firstname,
                "picture": result.rows[0].user_picture,
            },
            "id": result.rows[0].id,
            "name": result.rows[0].name,
            "description": result.rows[0].description,
            "start_date": result.rows[0].start_date,
            "end_date": result.rows[0].end_date
        }*/
    };
    return JSON.stringify(response);
}

//global var
global.sortBy = ['name', 'start_date', 'end_date', 'client', 'site', '-name', '-start_date', '-end_date', '-client', '-site'];
global.sqlSortSyntax = ['name asc', 'start_date asc', 'end_date asc', 'client asc', 'site asc', 'name desc', 'start_date desc', 'end_date desc', 'client desc', 'site desc'];
global.searchSyntax = "where name ~* '%{search}'";
global.pageSize = 10;

async function resourceSwitch(event) {
    switch (event.resource) {

        case '/missions/{m_id}':
            if (!event.pathParameters.m_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createSingleResponse(event.pathParameters.m_id, event.path);

        case '/users/{u_id}/missions':
            if (!event.pathParameters.u_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createPaginatedResponse(event, `select * from users_get_missions(${event.pathParameters.u_id})`);

        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

exports.handler = async (event, context, callback) => {
    // psql client
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    await resourceSwitch(event).then((data) => {
        console.log(data);
        callback(null, {
            statusCode: 200,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });

    // end psql
    global.pgPool.end();
};