var webpack = require('webpack');
var isProduction = process.env.NODE_ENV === 'production';

var plugins = [];
plugins.push(
    /**
     * IgnorePlugin will skip any require
     * that matches the following regex.
     */
    new webpack.IgnorePlugin(/^pg-native$/)
);

module.exports = {
    entry: './index.js',
    target: 'node',
    mode: 'none',
    plugins: plugins
  };