/*

API Gateway resolver

runtime : nodejs8.10

env vars : CONNECTION_STRING

psql user : apicommon  

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

switch attached_API_endpoints :
    - GET /users
    - GET /users/{u_id}
    - GET /sites/{s_id}/users

returns client object : 
{
    "id": "userId",
    "familyname": "userName",
    "firstname": "userFirstname",
    "nickname": "userNickname",
    "birthdate" "userBirthdate",
    "email": "userEmail",
    "phone": "userPhone",
    "picture": "userPicture",
    "descr": "userDescription",
    "date_in": "userHireDate",
    "pos": "userRole",
    "mission": [{     // used to distinguish user who's still working on a site and user who's been working
        "start_date": "missionStartDate",
        "end_date": "missionEndDate"
    }]
}

*/

'use strict';

const mainModule = require('mainModule');
const pg = require('pg');

async function getUsersMissions(userId, event) {
    if (event.resource === '/sites/{s_id}/users') {
        const missions = await mainModule.pgQuery(`SELECT * FROM users_get_sites_missions(${userId},${event.pathParameters.s_id}) ORDER BY end_date DESC, start_date DESC;`);
        return missions.rows;
    } else {
        return null;
    }
}

// create paginated response (GET /users  &  GET /sites/{s_id}/users)
async function createPaginatedResponse(event, query) {
    // check query string parameters (page, search, sort)
    const params = await mainModule.resolveParams(event.queryStringParameters).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: err.callbackStatusCode,
            callbackBody: err.callbackBody
        });
    });
    // query DB
    const result = await mainModule.pgQuery(`${query} ${params.sort} ${params.search} limit ${global.pageSize} offset ${params.page * global.pageSize};`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 400 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }

    let endResult = [];
    for (let i in result.rows) {
        endResult.push({
            "id": result.rows[i].id,
            "familyname": result.rows[i].lastname,
            "firstname": result.rows[i].firstname,
            "nickname": null,
            "birthdate": result.rows[i].birthdate,
            "email": result.rows[i].email,
            "phone": result.rows[i].phone,
            "picture": result.rows[i].picture,
            "descr": result.rows[i].description,
            "date_in": result.rows[i].hire_date,
            "pos": result.rows[i].role,
            "prophone": result.rows[i].prophone,
            "title": result.rows[i].title,
            "country": result.rows[i].country,
            "mission": await getUsersMissions(result.rows[i].id, event)
        });
    }

    const count = await mainModule.pgQuery(`${query.replace('*', 'count(id)')} ${params.search}`);

    const response = {
        "_links": await mainModule.createJsonObject(event.path, params.page, `${query.replace('*', 'count(id)')} ${params.search};`),
        "size": result.rows.length,
        "start": params.page * global.pageSize + 1,
        "count": count.rows[0].count,
        "results": endResult
    };
    console.log(response);
    return JSON.stringify(response);
}

async function createSingleResponse(id, path) {

    const result = await mainModule.pgQuery(`select * from users_get_byid(${id});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }

    const response = {
        "_links": {
            "self": path
        },
        "results": [{
            "id": result.rows[0].id,
            "familyname": result.rows[0].lastname,
            "firstname": result.rows[0].firstname,
            "nickname": null,
            "birthdate": result.rows[0].birthdate,
            "email": result.rows[0].email,
            "phone": result.rows[0].phone,
            "picture": result.rows[0].picture,
            "descr": result.rows[0].description,
            "date_in": result.rows[0].hire_date,
            "pos": result.rows[0].role,
            "prophone": result.rows[0].prophone,
            "title": result.rows[0].title,
            "country": result.rows[0].country,
            "mission": null
        }] //in future, list will be removed
    };
    return JSON.stringify(response);
}

//global var
global.sortBy = ['lastname', 'firstname', 'role', 'end_date', '-lastname', '-firstname', '-role', '-end_date'];
global.sqlSortSyntax = ['lastname asc', 'firstname asc', 'role asc', 'end_date asc', 'lastname desc', 'firstname desc', 'role desc', 'end_date desc'];
global.searchSyntax = "where lastname ~* '%{search}' or firstname ~* '%{search}'";
global.pageSize = 1000;

async function resourceSwitch(event) {
    switch (event.resource) {
        case '/users':
            return await createPaginatedResponse(event, 'select * from users_get()');

        case '/users/{u_id}':
            if (!event.pathParameters.u_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createSingleResponse(event.pathParameters.u_id, event.path);

        case '/sites/{s_id}/users':
            if (!event.pathParameters.s_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createPaginatedResponse(event, `select * from sites_get_users(${event.pathParameters.s_id})`);

        case '/users/list':
            const result = await mainModule.pgQuery(`SELECT users.id,users.firstname,users.lastname,users.email,roles.name FROM users,roles WHERE roles.id=users.role AND users.id NOT IN (${event.notInStatement});`);
            return result;

        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

exports.handler = async (event, context, callback) => {
    // psql client
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    await resourceSwitch(event).then((data) => {
        console.log(data);
        callback(null, {
            statusCode: 200,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });

    // end psql
    global.pgPool.end();
};