/*

API Gateway resolver

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING

psql user : userpost 

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

attached_API_endpoints : PUT /users/{u_id}

returns user object : 
{
}

*/

'use strict';

const pg = require('pg');

// function to query DB
async function pgQuery(query) {
    const DBclient = new pg.Client(process.env.CONNECTION_STRING);

    await DBclient.connect();

    const result = await DBclient.query(query);

    await DBclient.end();

    return result;
}

// function to insert char ' in DB
async function handleSingleQuote(string) {
    return string.replace(/'/g, `''`);
}

// handle null value
async function handleNullParam(value) {
    if (value) {
        return `'${await handleSingleQuote(value)}'`;
    } else {
        return null;
    }
}

// update user's profile
async function main(body, userId) {
    // check 400 error
    if (!userId) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }
    // update user's profile in DB
    console.log(`select * from users_put(${userId},${await handleNullParam(body[validParam[0]])},${await handleNullParam(body[validParam[1]])},${await handleNullParam(body[validParam[2]])},null);`);
    const updatedUser = await pgQuery(`select * from users_put(${userId},${await handleNullParam(body[validParam[0]])},${await handleNullParam(body[validParam[1]])},${await handleNullParam(body[validParam[2]])},null);`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // return JSON formatted response
    return JSON.stringify(updatedUser.rows[0]);
}

const validParam = ['phone', 'descr', 'birthdate'];

exports.handler = async (event, context, callback) => {
    await main(JSON.parse(event.body), event.pathParameters.u_id).then((data) => {
        console.log('ok');
        callback(null, {
            statusCode: 201,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });
};  
