/*

API Gateway resolver

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING
    - BUCKET_NAME

psql user : userpost 

IAM role : Lambda_API_users_POST

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations
    - S3 put / delete object

attached_API_endpoints : POST /users/{u_id}

returns user object : 
{
}

*/

'use strict';

const AWS = require('aws-sdk');
const multipart = require('parse-multipart');
const pg = require('pg');
const jwt = require('jsonwebtoken');


// function to query DB
async function pgQuery(query) {
    const DBclient = new pg.Client(process.env.CONNECTION_STRING);

    await DBclient.connect();

    const result = await DBclient.query(query);

    await DBclient.end();

    return result;
}

// function to get file's extension
function getExtension(fileName) {
    const fileNameArray = fileName.split('.');

    return fileNameArray[fileNameArray.length - 1];
}

// formdata parser
function getFormData(event) {
    const boundary = multipart.getBoundary(event.headers['content-type']);

    const bodyBuffer = new Buffer(event.body.toString(), 'base64');

    const res = multipart.Parse(bodyBuffer, boundary);

    return res;
}

// delete object in s3
function deletePreviousPicture(objectName) {
    return new Promise((resolve, reject) => {
        const s3 = new AWS.S3();
        s3.deleteObject({ Bucket: process.env.BUCKET_NAME, Key: `users/${objectName}` }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// verify if user has the default picture
async function verifyVersion(id) {
    const oldPicture = await pgQuery(`select picture from users_get_byid(${id});`);

    if (oldPicture.rows[0].picture != 'default_profile_picture.png') {
        await deletePreviousPicture(oldPicture.rows[0].picture).then((data) => {
            console.log(data);
        }).catch((err) => {
            console.error(err);
        });
    } else {
        return;
    }
}

// upload a picture to s3
function putObject(params) {
    const s3 = new AWS.S3();
    return new Promise((resolve, reject) => {
        s3.putObject(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function to verify user's identity in token
function verifyJWT(token, userId) {
    const decoded = jwt.decode(token);
    if (decoded['custom:id'] === userId) {
        return true;
    } else {
        return false;
    }
}



// main function
async function main(event) {
    // 400 error
    if (!event.pathParameters.u_id) {
        console.error('missing u_id');
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    // 403 error
    if (!await verifyJWT(event.headers.Authorization, event.pathParameters.u_id)){
        return Promise.reject({
            callbackStatusCode: 403,
            callbackBody: JSON.stringify('Forbiden')
        });
    }

    // parse formdata
    const formdata = await getFormData(event);

    /*if (formdata[0].type.split('/')[0] != 'image') {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }*/

    // delete user's old picture in s3
    await verifyVersion(event.pathParameters.u_id);

    // get file extension
    const extension = await getExtension(formdata[0].filename);
    
    // update users.picture in db
    await pgQuery(`select * from users_put(${event.pathParameters.u_id},CAST(null AS VARCHAR),CAST(null AS VARCHAR),CAST(null AS VARCHAR),'${event.pathParameters.u_id}.${extension}');`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    // upload new picture to s3
    const params = {
        Body: formdata[0].data,
        Bucket: process.env.BUCKET_NAME,
        Key: `users/${event.pathParameters.u_id}.${extension}`
    };
    await putObject(params).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    
    return;
}

exports.handler = async (event, context, callback) => {
    // wait main, catch error
    await main(event).then((data) => {
        callback(null, {
            statusCode: 201,
            body: JSON.stringify('Created'),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });
};