/*

API Gateway resolver - feedback for beta versions

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING
    - 

psql user : feedbackpost

IAM role : Lambda_API_events_PUT

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations
    - SNS publish

switch attached_API_endpoints :
    - POST /users/{u_id}/feebacks

*/

'use strict';

const pg = require('pg');
const AWS = require('aws-sdk');

async function pgQuery(query) {
    const DBclient = new pg.Client(process.env.CONNECTION_STRING);

    await DBclient.connect();

    const result = await DBclient.query(query);

    await DBclient.end();

    return result;
}

// function to pusblish to sns
function publish(payload) {
    const sns = new AWS.SNS();

    return new Promise((resolve, reject) => {
        sns.publish({
            Message: payload,
            TopicArn: process.env.NOTIFICATION_TOPIC_ARN
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

function handleSingleQuote(string){
    return string.replace(/'/g, `''`).trim();
}

async function main(body, userId) {
    if (!body || !userId) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: 'Bad request'
        });
    }

    if (!body.content) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: 'Bad request'
        });
    }

    const res = await pgQuery(`SELECT * FROM feedbacks_put(${userId},'${await handleSingleQuote(body.content)}');`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: 'server error'
        });
    });

    const payload = JSON.stringify({
        "type": "pushFeedback",
        "feedbackData": {
            "userId": userId,
            "content": body.content
        }
    });
    // pusblish to sns to invoke lambda notification pusher
    await publish(payload).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    return res;
}

exports.handler = async (event, context, callback) => {
    console.log(`Feedback : userId=${event.pathParameters.u_id}, message='${JSON.parse(event.body).content}'`);

    await main(JSON.parse(event.body), event.pathParameters.u_id).then(() => {
        callback(null, {
            statusCode: 201,
            body: JSON.stringify('Created'),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: JSON.stringify(err.callbackBody),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });
};