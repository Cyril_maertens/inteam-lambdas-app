/*

API Gateway resolver

runtime : nodejs8.10

env vars : CONNECTION_STRING

psql user : apicommon

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

VPC config : standard

switch attached_API_endpoints :
    - GET /clients
    - GET /clients/{c_id}

returns client object : 
{
    "id": "clientId",
    "name": "clientName",
    "logo": "clientLogo"
}

*/

'use strict';

const pg = require('pg');
const mainModule = require('mainModule');

// create paginated response (GET /clients)
async function createPaginatedResponse(event, query) {
    // check query string parameters (page, search, sort)
    const params = await mainModule.resolveParams(event.queryStringParameters).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: err.callbackStatusCode,
            callbackBody: err.callbackBody
        });
    });
    // query DB
    const result = await mainModule.pgQuery(`${query} ${params.sort} ${params.search} limit ${global.pageSize} offset ${params.page * global.pageSize};`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 404 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }
    // format result to be returned
    let endResult = [];
    for (let i in result.rows) {
        endResult.push({
            "id": result.rows[i].id,
            "name": result.rows[i].name,
            "logo": result.rows[i].logo,
            "countries": result.rows[i].countries
        });
    }
    // format REST API response
    const count = await mainModule.pgQuery(`${query.replace('*', 'count(id)')} ${params.search}`);

    const response = {
        "_links": await mainModule.createJsonObject(event.path, params.page, `${query.replace('*', 'count(id)')} ${params.search};`),
        "size": result.rows.length,
        "start": params.page * global.pageSize,
        "count": count.rows[0].count,
        "results": endResult
    };
    // return JSON formatted response
    return JSON.stringify(response);
}

// create single response (GET /clients/{c_id})
async function createSingleResponse(id, path) {
    // query DB
    const result = await mainModule.pgQuery(`select * from clients_get_byid(${id});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 404 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }
    // format REST API response
    const response = {
        "_links": {
            "self": path
        },
        "results": result.rows[0]
    };
    // return JSON formatted response
    return JSON.stringify(response);
}


//global var

// authorized sort query string parameter
global.sortBy = ['name', '-name'];
// sql sort syntax paramters
global.sqlSortSyntax = ['name asc', 'name desc'];
// sql search syntax parameters
global.searchSyntax = "where name ~* '%{search}'";
// page size for paginated response
global.pageSize = 50;

// main function, switch between API endpoints
async function resourceSwitch(event) {
    switch (event.resource) {
        // GET /clients
        case '/clients':
            return await createPaginatedResponse(event, 'select * from clients_get()');

        // GET /clients/{c_id}
        case '/clients/{c_id}':
            if (!event.pathParameters.c_id) // check for 400 error
            {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createSingleResponse(event.pathParameters.c_id, event.path);

        // unused API endpoint
        //case '/users/{u_id}/clients':
        //    if (!event.pathParameters.u_id) {
        //        return Promise.reject({
        //            callbackStatusCode: 400,
        //            callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
        //        });
        //    }
        //    return await createPaginatedResponse(event, `select * from users_get_clients(${event.pathParameters.u_id})`);

        // 400 error
        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

// handler executed when lambda is invoked
exports.handler = async (event, context, callback) => {
    // psql client pool
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });
    // swith between api endpoints, catch error
    await resourceSwitch(event).then((data) => {
        console.log(data);
        callback(null, {
            statusCode: 200,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });

    // end psql
    global.pgPool.end();
};