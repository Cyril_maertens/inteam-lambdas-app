/*

API Gateway resolver

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING
    - NOTIFICATION_TOPIC_ARN

psql user : eventpost

IAM role : Lambda_API_events_PUT

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations
    - SNS publish push_notification_topic

VPC config : standard

switch attached_API_endpoints :
    - PUT /users/{v_id}/events
    - PUT /users/{v_id}/events/{v_id}

returns event object : 
{
    
}

*/

'use strict';

const pg = require('pg');
const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');

// function to query db
async function pgQuery(query) {
    const result = await global.pgPool.query(query);
    return result;
}

// function to insert char ' in db
function handleSingleQuote(string) {
    return string.replace(/'/g, `''`).trim();
}

// function to handle null value
function handleNullParam(value) {
    if (value) {
        return `${handleSingleQuote(value)}`;
    } else {
        return null;
    }
}

// function to parse users' token sent to sns topic
async function parseTokens(invitedUsers) {
    let result = [], string = '';
    for (let i in invitedUsers) {
        string += `${invitedUsers[i].id},`;
    }
    string = string.substring(0, string.length - 1);

    const tokens = await pgQuery(`SELECT token FROM notifications WHERE "user" IN (${string});`);

    for (let i in tokens.rows) {
        result.push(tokens.rows[i].token);
    }

    return result;
}

// function to pusblish to sns
function publish(payload) {
    const sns = new AWS.SNS();

    return new Promise((resolve, reject) => {
        sns.publish({
            Message: payload,
            TopicArn: process.env.NOTIFICATION_TOPIC_ARN
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function to verify user's identity in token
function verifyJWT(token, userId) {
    const decoded = jwt.decode(token);
    if (decoded['custom:id'] === userId) {
        return true;
    } else {
        return false;
    }
}

// function to verify user's role in token
function verifyRole(token) {
    const decoded = jwt.decode(token);
    if (decoded['custom:role'] === 'commercial') {
        return true;
    } else {
        return false;
    }
}

// create a new event
async function createEvent(body, userId, token) {
    // check 403 error (only sales man can create an event)
    if (!await verifyRole(token)) {
        return Promise.reject({
            callbackStatusCode: 403,
            callbackBody: JSON.stringify('Forbidden')
        });
    }
    // check 403, verify user's identity
    if (!await verifyJWT(token, userId)) {
        return Promise.reject({
            callbackStatusCode: 403,
            callbackBody: JSON.stringify('Forbidden')
        });
    }

    // check 400 errors
    if (!body) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    if (!body.emit_date || !body.title || !body.site) {
        console.error('400 : Bad request');
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    //check 403 (cannot create event on Ineat's sites)
    /*const client = await pgQuery(`SELECT clients.name FROM clients,sites WHERE clients.id=sites.client AND sites.id=${body.site}`);
    if (client.rows[0].name.toLowerCase() === 'ineat') {
        return Promise.reject({
            callbackStatusCode: 403,
            callbackBody: JSON.stringify('Forbidden')
        });
    }*/

    // insert new event in db
    const newEvent = await pgQuery(`SELECT * from events_put(${userId},'${body.emit_date}','${await handleSingleQuote(body.title)}','${await handleNullParam(body.content)}',${body.site});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    // get users invited to event (will changed in v2)
    const invitedUsers = await pgQuery(`SELECT id FROM sites_get_users_count(${body.site});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // link users to event in db
    for (let i in invitedUsers.rows) {
        await pgQuery(`INSERT INTO user_event ("user",event) VALUES (${invitedUsers.rows[i].id},${newEvent.rows[0].id});`).catch((err) => {
            console.error(err);
            return Promise.reject({
                callbackStatusCode: 500,
                callbackBody: JSON.stringify('Internal server error')
            });
        });
    }

    // get event's creator info
    const emiter = await pgQuery(`SELECT lastname,firstname FROM users_get_byid(${userId});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    // format data to pusblish to sns
    const payload = JSON.stringify({
        "type": "pushEvent",
        "eventData": {
            "id": newEvent.rows[0].id,
            "title": newEvent.rows[0].title,
            "content": newEvent.rows[0].content,
            "emit_date": newEvent.rows[0].date,
            "sender_familyname": emiter.rows[0].lastname,
            "sender_firstname": emiter.rows[0].firstname,
            "from_user": userId,
            "site": newEvent.rows[0].site
        },
        "invitedUsers": await parseTokens(invitedUsers.rows)
    });

    // pusblish to sns to invoke lambda notification pusher
    await publish(payload).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    const response = {
        "id": newEvent.rows[0].id,
        "from_user": newEvent.rows[0].user,
        "date_when": newEvent.rows[0].date,
        "title": newEvent.rows[0].title,
        "content": newEvent.rows[0].content,
        "site": newEvent.rows[0].site,
        //"users": invitedUsers.rows  // used in v2
    };
    // return JSON formatted response
    return JSON.stringify(response);
}
/*  used in v2 for users' response
async function updateEvent(body, userId, eventId) {
    if (!body.userChoice || !userId || !eventId || [-1, 0, 1].indexOf(body.userChoice) === -1) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }
    
    await pgQuery(`UPDATE user_event SET user_response=CAST(${body.userChoice} AS INT) WHERE user=${userId} AND event=${eventId};`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    const result = await pgQuery(`select * from events_get_byid(${eventId});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    const invitedUsers = await pgQuery(`select * from events_get_users(${result.rows[0].id});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    const response = {
        "id": result.rows[0].id,
        "from_user": result.rows[0].from_user,
        "date_when": result.rows[0].date_when,
        "title": result.rows[0].title,
        "content": result.rows[0].content,
        "client": {
            "id": result.rows[0].client_id,
            "name": result.rows[0].client_name,
            "logo": result.rows[0].client_logo
        },
        "site": {
            "id": result.rows[0].site_id,
            "name": result.rows[0].site_name
        },
        "sender": {
            "id": result.rows[0].sender_id,
            "familyname": result.rows[0].sender_familyname,
            "firstname": result.rows[0].sender_firstname,
            "email": result.rows[0].sender_email,
            "picture": result.rows[0].sender_picture
        },
        "users": invitedUsers.rows
    };

    return JSON.stringify(response);
}*/


// function to swith between api endpoints
async function resourceSwitch(event) {
    switch (event.resource) {
        //case '/users/{u_id}/sites/{s_id}/events': //future route
        // PUT /users/{u_id}/events
        case '/users/{u_id}/events':
            if (!event.pathParameters.u_id) // check 400 error
            {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createEvent(JSON.parse(event.body), event.pathParameters.u_id, event.headers.Authorization);

        // PUT /users/{u_id}/events/{v_id}  
        /*case 'users/{u_id}/events/{v_id}':
            return await updateEvent(JSON.parse(event.body), event.pathParameters.u_id, event.pathParameters.v_id);
*/
        // 400 error
        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

exports.handler = async (event, context, callback) => {
    // psql client pool
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });
    // switch between api endpoints, catch error
    await resourceSwitch(event).then((data) => {
        console.log(data);
        callback(null, {
            statusCode: 201,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        console.error(err);
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });

    // end psql
    global.pgPool.end();
};