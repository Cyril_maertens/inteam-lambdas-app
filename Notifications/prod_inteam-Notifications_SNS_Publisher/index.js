/*

SNS publisher (v0 : this version uses google api instead of AWS SNS)

runtime : nodejs8.10

env vars : 
    - SLACK_URL
    - AUTHORIZATION      // in v2 replace with SNS_TOPIC_BASE_ARN
    - URL                //

psql user : snspublisher

IAM role : Lambda_SNS_publisher

IAM authorizations : 
    - Cloudwatch logs   // in v2 add SNS:publish

VPC config : none

switch attached_API_endpoints : 
    - getAllConversations
    - getConversation
    - newConversation
    - newMessage

*/

'use strict';

const axios = require('axios');   // not used in v2

// publish notification in case of a new event
async function publishEventNotification(eventData, invitedUsers) {
    // notification's data
    const notification = {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "id": `${eventData.id}`,
        "status": "done",
        "type": "event",
        "data": {
            "id": +eventData.id,
            "title": eventData.title,
            "content": eventData.content,
            "emit_date": eventData.emit_date,
            "sender_familyname": eventData.sender_familyname,
            "sender_firstname": eventData.sender_firstname,
            "from_user": +eventData.from_user,
            "site": +eventData.site
        }
    };


    // call google api to push notification
    const params = {
        url: process.env.URL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': process.env.AUTHORIZATION
        },
        data: {
            "registration_ids": invitedUsers,
            "priority": "high",
            "notification": {
                "body": `${eventData.title} (${eventData.content})`,
                "title": `Nouvel évènement de ${eventData.sender_firstname} ${eventData.sender_familyname}`
            },
            "data": notification
        }
    };

    console.log(`Pushing event notification : Success if no error.`);

    await axios(params).then((data) => {
        console.log(data);
    }).catch((err) => {
        console.error(err);
    });

}

// publish notification in case of a new message
async function publishMessageNotification(messageData, toUser) {
    // notification's data
    const notification = {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "id": `${messageData.id}`,
        "status": "done",
        "type": "message",
        "data": {
            "id": +messageData.id,
            "content": messageData.content,
            "emit_date": messageData.emit_date,
            "sender_familyname": messageData.sender_familyname,
            "sender_firstname": messageData.sender_firstname,
            "from_user": +messageData.from_user,
            "to_user": +messageData.to_user,
            "is_sent": +messageData.is_sent,
            "is_seen": +messageData.is_seen,
            "owned": +messageData.owned
        }
    };
    // call google api
    const params = {
        url: process.env.URL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': process.env.AUTHORIZATION
        },
        data: {
            "registration_ids": toUser,
            "priority": "high",
            "notification": {
                "body": messageData.content,
                "title": `Nouveau message de ${messageData.sender_firstname} ${messageData.sender_familyname}`
            },
            "data": notification
        }
    };

    console.log(`Pushing message notification : Success if no error.`);

    await axios(params).then((data) => {
        console.log(data);
    }).catch((err) => {
        console.error(err);
    });
}

async function publishFeedbackNotification(feedbackData) {
    const payload = {
        "icon_emoji": ":mailbox_with_mail:",
        "username": `Feedback user:${feedbackData.userId}, ${new Date().toLocaleString()}`,
        "text": `${feedbackData.content}`
    };

    const params = {
        url: process.env.SLACK_URL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: payload
    };

    await axios(params).then((data) => {
        console.log(data);
    }).catch((err) => {
        console.error(err);
    });
}

// main, switch between event notification or message notification
exports.handler = async (event, context, callback) => {
    const payload = JSON.parse(event.Records[0].Sns.Message);
    switch (payload.type) {

        case 'pushEvent':
            console.log('pushEvent');
            await publishEventNotification(payload.eventData, payload.invitedUsers);
            break;

        case 'pushMessage':
            console.log('pushMessage');
            await publishMessageNotification(payload.messageData, payload.toUser);
            break;

        case 'pushFeedback':
            console.log('pushFeedback');
            await publishFeedbackNotification(payload.feedbackData);
            break;

        default:
            console.error('400 Bad request');
            break;
    }
};