/*

API Gateway resolver

runtime : nodejs8.10

env vars : CONNECTION_STRING

psql user : apicommon

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

VPC config : standard

switch attached_API_endpoints :
    - PUT /users/{u_id}/notifications
    - DELETE /users/{u_id}/notifications

*/

'use strict';

const pg = require('pg');

// function to query DB
async function pgQuery(query) {
    const DBclient = new pg.Client(process.env.CONNECTION_STRING);

    await DBclient.connect();

    const result = await DBclient.query(query).catch((err) => {
        return Promise.reject(err);
    });

    await DBclient.end();

    return result;
}

async function insertToken(userId, body) {
    if (!body) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    if (!body.token) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    const res = await pgQuery(`SELECT * FROM notifications_put_token(${userId},'${body.token}','${new Date().toISOString()}');`);

    if (res.rows[0]) {
        return {
            callbackStatusCode: 201,
            callbackBody: JSON.stringify('Created')
        };
    } else {
        return {
            callbackStatusCode: 208,
            callbackBody: JSON.stringify('Already Reported')
        };
    }
}

async function deleteToken(userId, body) {
    if (!body) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    if (!body.token) {
        return Promise.reject({
            callbackStatusCode: 400,
            callbackBody: JSON.stringify('Bad request')
        });
    }

    await pgQuery(`SELECT * FROM notifications_delete_token(${userId},'${body.token}');`);

    return {
        callbackStatusCode: 204,
        callbackBody: JSON.stringify('Deleted')
    };
}

async function resourceSwitch(event) {
    switch (event.httpMethod) {
        case 'POST':
            if (!event.pathParameters.u_id) // check for 400 error
            {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await insertToken(event.pathParameters.u_id, JSON.parse(event.body));

        case 'DELETE':
            if (!event.pathParameters.u_id) // check for 400 error
            {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await deleteToken(event.pathParameters.u_id, JSON.parse(event.body));

        // 400 error
        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

exports.handler = async (event, context, callback) => {
    await resourceSwitch(event).then((data) => {
        callback(null, {
            statusCode: data.callbackStatusCode,
            body: data.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });
};