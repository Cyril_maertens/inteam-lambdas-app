/*

API Gateway resolver

runtime : nodejs8.10

env vars : CONNECTION_STRING

psql user : apicommon

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

switch attached_API_endpoints :
    - GET /sites/{s_id}
    - GET /clients/{c_id}/sites

returns site object : 
{

}

*/

'use strict';

const mainModule = require('mainModule');
const pg = require('pg');

// create paginated response   GET /clients/{c_id}/sites
async function createPaginatedResponse(event, query) {
    // check query string parameters (page, search, sort)
    const params = await mainModule.resolveParams(event.queryStringParameters).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: err.callbackStatusCode,
            callbackBody: err.callbackBody
        });
    });
    // query DB
    const result = await mainModule.pgQuery(`${query} ${params.sort} ${params.search} limit ${global.pageSize} offset ${params.page * global.pageSize};`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 404 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }
    // format result to be returned
    let endResult = [];
    for (let i in result.rows) {

        const tempResults = await Promise.all([
            mainModule.pgQuery(`SELECT COUNT(id) FROM sites_get_users_count(${result.rows[i].id});`),
            mainModule.pgQuery(`SELECT id,lastname,firstname,email,picture FROM users_get_byid(${result.rows[i].sales_man});`)
        ]);

        endResult.push(
            {
                "client_id": result.rows[i].client_id,
                "client": result.rows[i].client_name,
                "client_logo": result.rows[i].client_logo,
                "id": result.rows[i].id,
                "site": result.rows[i].name,
                "num": result.rows[i].num,
                "street": result.rows[i].street,
                "code": result.rows[i].code,
                "city": result.rows[i].city,
                "complement": result.rows[i].address_2,
                "phone": result.rows[i].phone,
                "trader_id": result.rows[i].sales_man,
                /*"trader_familyname": tempResults[1].rows[0].lastname,
                "trader_firstname": tempResults[1].rows[0].firstname,
                "trader_email": tempResults[1].rows[0].email,
                "trader_picture": tempResults[1].rows[0].picture,*/
                "userCount": tempResults[0].rows[0].count,
                "latitude": result.rows[i].latitude,
                "longitude": result.rows[i].longitude
            }
            /*{
            "client": {
                "id": result.rows[i].client_id,
                "name": result.rows[i].client_name,
                "logo": result.rows[i].client_logo
            },
            "sales_man": tempResults[1].rows[0],
            "id": result.rows[i].id,
            "name": result.rows[i].name,
            "num": result.rows[i].num,
            "street": result.rows[i].street,
            "code": result.rows[i].code,
            "city": result.rows[i].city,
            "address_2": result.rows[i].address_2,
            "phone": result.rows[i].phone,
            "userCount": tempResults[0].rows[0].count,
        }*/
        );
    }
    // format REST API response
    const count = await mainModule.pgQuery(`${query.replace('*', 'count(id)')} ${params.search}`);

    const response = {
        "_links": await mainModule.createJsonObject(event.path, params.page, `${query.replace('*', 'count(id)')} ${params.search};`),
        "size": result.rows.length,
        "start": params.page * global.pageSize,
        "count": count.rows[0].count,
        "results": endResult
    };
    // return JSON formatted response
    return JSON.stringify(response);
}

// create single response   GET /sites/{s_id}
async function createSingleResponse(id, path) {
    // query db
    const result = await mainModule.pgQuery(`select * from sites_get_byid(${id});`).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });
    // check 404 error
    if (!result.rows[0]) {
        return Promise.reject({
            callbackStatusCode: 404,
            callbackBody: JSON.stringify('Not found')
        });
    }
    // format result to be returned
    const tempResults = await Promise.all([
        mainModule.pgQuery(`SELECT * FROM sites_get_users_count(${result.rows[0].id});`),
        mainModule.pgQuery(`SELECT id,lastname,firstname,email,picture FROM users_get_byid(${result.rows[0].sales_man});`)
    ]);

    const response = {
        "_links": {
            "self": path
        },
        "results":
            {
                "client_id": result.rows[0].client_id,
                "client": result.rows[0].client_name,
                "client_logo": result.rows[0].client_logo,
                "id": result.rows[0].id,
                "site": result.rows[0].name,
                "num": result.rows[0].num,
                "street": result.rows[0].street,
                "code": result.rows[0].code,
                "city": result.rows[0].city,
                "complement": result.rows[0].address_2,
                "phone": result.rows[0].phone,
                "trader_id": result.rows[0].sales_man,
                /*"trader_familyname": tempResults[1].rows[0].lastname,
                "trader_firstname": tempResults[1].rows[0].firstname,
                "trader_email": tempResults[1].rows[0].email,
                "trader_picture": tempResults[1].rows[0].picture,*/
                "userCount": tempResults[0].rows[0].count,
                "latitude": result.rows[0].latitude,
                "longitude": result.rows[0].longitude
            }
        /*{
        "client": {
            "id": result.rows[0].client_id,
                "name": result.rows[0].client_name,
                    "logo": result.rows[0].client_logo
        },
        "sales_man": salesMan.rows[0],
            "id": result.rows[0].id,
                "name": result.rows[0].name,
                    "num": result.rows[0].num,
                        "street": result.rows[0].street,
                            "code": result.rows[0].code,
                                "city": result.rows[0].city,
                                    "address_2": result.rows[0].address_2,
                                        "phone": result.rows[0].phone
    }*/
    };
    // return JSON formatted response
    return JSON.stringify(response);
}

//global var
global.sortBy = ['name', 'client', 'city', '-name', '-client', '-city'];
global.sqlSortSyntax = ['name asc', 'client asc', 'city asc', 'name desc', 'client desc', 'city desc'];
global.searchSyntax = "where name ~* '%{search}' or city ~* '%{search}'";
global.pageSize = 20;

async function resourceSwitch(event) {
    switch (event.resource) {

        case '/sites/{s_id}':
            if (!event.pathParameters.s_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createSingleResponse(event.pathParameters.s_id, event.path);

        case '/clients/{c_id}/sites':
            if (!event.pathParameters.c_id) {
                return Promise.reject({
                    callbackStatusCode: 400,
                    callbackBody: JSON.stringify('Bad request: Missing path parameter(s)')
                });
            }
            return await createPaginatedResponse(event, `select * from clients_get_sites(${event.pathParameters.c_id})`);

        default:
            return Promise.reject({
                callbackStatusCode: 400,
                callbackBody: JSON.stringify('Bad request')
            });
    }
}

exports.handler = async (event, context, callback) => {
    // psql client
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    await resourceSwitch(event).then((data) => {
        console.log(data);
        callback(null, {
            statusCode: 200,
            body: data,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    }).catch((err) => {
        callback(null, {
            statusCode: err.callbackStatusCode,
            body: err.callbackBody,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        });
    });

    // end psql
    global.pgPool.end();
};