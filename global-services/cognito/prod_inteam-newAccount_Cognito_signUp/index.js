/*

Cognito signUp - get users in db

runtime : nodejs8.10

env vars : 
    - USERPOOL_ID
    - PROVIDER_NAME
    - GET_USERS_FUNCTION_NAME

psql user : apicommon

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations
    - Lambda invoke

VPC config = none

*/

'use strict';

const AWS = require('aws-sdk');

// get users who already have an account
function getCognitoUsers() {
    return new Promise((resolve, reject) => {

        const cognitoIdentity = new AWS.CognitoIdentityServiceProvider();

        const params = {
            UserPoolId: process.env.USERPOOL_ID,
            AttributesToGet: [
                'custom:id'
            ]
        };

        cognitoIdentity.listUsers(params, (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                let result = [];
                for (let i in data.Users) {
                    result.push(data.Users[i].Attributes[0].Value);
                }
                resolve(result);
            }
        });
    });
}

//invoke lambda to get users in db
function getDbUsers(params) {
    return new Promise((resolve, reject) => {
        const lambda = new AWS.Lambda();

        lambda.invoke(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// list users who need a new account
async function getUsersToCreate() {
    // get users from Cognito
    const cognitoUsers = await getCognitoUsers();

    let string = '';
    for (let i in cognitoUsers) {
        string += `${cognitoUsers[i]},`;
    }
    string = string.substring(0, string.length - 1);

    // get users from DB
    const dbUsers = await getDbUsers({
        FunctionName: process.env.GET_USERS_FUNCTION_NAME,
        Payload: `{"resource":"/users/list", "notInStatement":"${string}"}`
    });
    console.log(dbUsers);
    const users = JSON.parse(dbUsers.Payload).body;
    console.log(users);
    let result = [];
    for (let i in users.rows) {
        result.push({
            "id": users.rows[i].id,
            "familyname": users.rows[i].lastname,
            "firstname": users.rows[i].firstname,
            "email": users.rows[i].email,
            "role": users.rows[i].role
        });
    }
    // return list of users who need an account
    return result;
}

// function to create account
const createCognitoUser = function (client, params) {
    return new Promise((resolve, reject) => {
        client.adminCreateUser(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

// function to link account to azure ad
const createCognitoLink = function (client, params) {
    return new Promise((resolve, reject) => {
        client.adminLinkProviderForUser(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};
/*
// function to create a topic in SNS
const createUserTopic = function (client, params) {
    return new Promise((resolve, reject) => {
        client.createTopic(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};
*/
// create a pwd required to create an account (this pwd will not be used)
/*async function createPwd(familyname, firstname) {
    return `${firstname.slice(0, 1).toUpperCase()}${firstname.slice(1, 3).toLowerCase()}${familyname.slice(0, 1).toUpperCase()}${familyname.slice(1, 3).toLowerCase()}57`;
}*/

// AWS apis clients
const cognitoIdentity = new AWS.CognitoIdentityServiceProvider();
//const sns = new AWS.SNS();

exports.handler = async (event, context, callback) => {
    const newUsers = await getUsersToCreate();
    /*const newUsers = [{
      "familyname": "MAERTENS", 
      "firstname": "Cyril",
      "email": "cyril.maertens@ineat-conseil.fr",
      "id": "108",
      "role": "Consultant"
    },
    {
      "familyname": "Chrétien", 
      "firstname": "Philippe",
      "email": "philippe.chretien@ineat-conseil.fr",
      "id": "189",
      "role": "Direction"
    },
    {
      "familyname": "Rossignol", 
      "firstname": "Pamela",
      "email": "pamela.rossignol@ineat-conseil.fr",
      "id": "254",
      "role": "Consultant"
    }];*/
    for (let i in newUsers) {
        let cognitoSub;

        await createCognitoUser(cognitoIdentity, {
            UserPoolId: process.env.USERPOOL_ID,
            Username: newUsers[i].email,
            MessageAction: "SUPPRESS",
            TemporaryPassword: "145MC45Yttt",
            UserAttributes: [
                {
                    Name: "family_name",
                    Value: `${newUsers[i].familyname}`
                },
                {
                    Name: "name",
                    Value: `${newUsers[i].firstname}`
                },
                {
                    Name: "email",
                    Value: `${newUsers[i].email}`
                },
                {
                    Name: "email_verified",
                    Value: "True"
                },
                {
                    Name: "custom:id",
                    Value: `${newUsers[i].id}`
                },
                {
                    Name: "custom:role",
                    Value: `${newUsers[i].role}`
                }
            ]
        }).then((data) => {
            cognitoSub = data.User.Username;
            console.log(data);
        }).catch((err) => {
            console.error(err);
        });

        await createCognitoLink(cognitoIdentity, {
            DestinationUser: {
                ProviderAttributeValue: cognitoSub,
                ProviderName: 'Cognito'
            },
            SourceUser: {
                ProviderAttributeName: 'email',
                ProviderAttributeValue: newUsers[i].email,
                ProviderName: process.env.PROVIDER_NAME
            },
            UserPoolId: process.env.USERPOOL_ID
        }).then((data) => {
            console.log(data);
        }).catch((err) => {
            console.log(err);
        });

        //await createUserTopic(sns, { Name: `userTopic_${newUsers[i].id}` }).then((data) => {
        //    console.log(data);
        //}).catch((err) => {
        //    console.log(err);
        //});
    }
};