/*

Cognito preSignUp handler - prevent auto creation of account from Azure AD

runtime : nodejs8.10

IAM role : Lambda_Basic_Execution

IAM authorizations : 
    - Cloudwatch logs

*/

'use strict';

// main, check signUp source
async function main(event) {
    if (event.triggerSource === 'PreSignUp_ExternalProvider') {
        return Promise.reject(null);
    } else {
        return;
    }
}

exports.handler = async (event, context, callback) => {
    console.log(event);
    main(event).then(
        data => callback(null, event)
    ).catch(
        err => callback(401, null)
    );

};