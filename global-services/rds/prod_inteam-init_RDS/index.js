/*

Initialize DB - invoked only once

runtime : nodejs8.10

IAM role : Lambda_init_RDS

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

ENV vars :
    - CONNECTION_STRING
    - BUCKET_NAME
    - OBJECT_KEY

VPC config : standard

*/

'use strict';

const AWS = require('aws-sdk');
const pg = require('pg');

// function to get an object in s3
function getObject(bucket, key) {
    const s3 = new AWS.S3();

    return new Promise((resolve, reject) => {
        s3.getObject({
            Bucket: bucket,
            Key: key
        }, (err, data) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function to delete an object in s3
function deleteObject(bucket, key) {
    const s3 = new AWS.S3();

    return new Promise((resolve, reject) => {
        s3.deleteObject({
            Bucket: bucket,
            Key: key
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function to parse sql file (may not work for all sql files)
function parseQueries(content, idx, queryArray) {
    let string = '';
    if (idx >= content.length - 1) {
        return queryArray;
    } else if (!content[idx].match(/\w/g)){
        return parseQueries(content, idx + 1, queryArray);
    } else if (content[idx].match(/FUNCTION/g)) {
        while (!content[idx].includes('$body$ LANGUAGE plpgsql;')) {
            string += `${content[idx].trim()} `;
            idx++;
        }
        string += `${content[idx].trim()} `;
        queryArray.push(string);
        return parseQueries(content, idx + 1, queryArray);
    } else if (content[idx].includes(';')) {
        string += `${content[idx].trim()} `;
        queryArray.push(string);
        return parseQueries(content, idx + 1, queryArray);
    } else {
        while (!content[idx].includes(';')) {
            string += `${content[idx].trim()} `;
            idx++;
        }
        string += `${content[idx].trim()} `;
        queryArray.push(string);
        return parseQueries(content, idx + 1, queryArray);
    }
}

exports.handler = async (event) => {
    // pg client pool

    const pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    // get init.sql file from s3
    const file = await getObject(process.env.BUCKET_NAME, process.env.OBJECT_KEY).catch((err) => {
        console.error(err);
    });
    console.log('--');
    // transform data into string and delete commented lines
    let content = file.Body.toString('utf8').replace(/(^(--).*)/gm, '');
    // split string into substrings
    content = content.split('\n');
    // parse each query in the file
    let tempArray = [], idx = 0;
    const queryArray = await parseQueries(content, idx, tempArray);
    console.log('queries');
    // execute each query
    for (let i in queryArray) {
        await pgPool.query(queryArray[i]).then((data)=>{
            console.log(data.command);
        }).catch((err)=>{
            console.error(err);
        });
    }
    console.log('end');
    // end pg client
    pgPool.end();
    // delete init.sql file in s3
    await deleteObject(process.env.BUCKET_NAME, process.env.OBJECT_KEY);

    return;
};