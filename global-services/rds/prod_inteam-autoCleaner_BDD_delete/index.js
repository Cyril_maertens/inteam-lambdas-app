/*

BDD cleaner

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING

psql user : cleaner

IAM role : Lambda_API_common

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations

event: cron rule in Cloudwatch

*/

'use strict';

const pg = require('pg');

// function to query db
async function pgQuery(query) {
    const result = await global.pgPool.query(query);

    return result;
}

// function to clean old messages in db (older than 28 days) 
async function cleanMessages() {
    const result = await pgQuery(`DELETE FROM messages WHERE date < (TIMESTAMP '${new Date().toISOString()}' - INTERVAL '672 hours');`); //28 days

    console.log(`${result.rowCount} messages have been deleted`);

    const conversations = await pgQuery(`SELECT id FROM conversations;`);

    for (let i in conversations.rows) {
        const messages = await pgQuery(`SELECT id FROM conversation_get_messages(${conversations.rows[i].id}) LIMIT 1;`);

        if (!messages.rows[0]) {
            await pgQuery(`DELETE FROM conversations WHERE id=${conversations.rows[i].id};`);
        }
    }

    return;
}

// function to clean old events (older than 7 days)
async function cleanEvents() {
    const result = await pgQuery(`DELETE FROM events WHERE date < (TIMESTAMP '${new Date().toISOString()}' - INTERVAL '168 hours');`); //7 days

    console.log(`${result.rowCount} events have been deleted`);

    return;
}

// function to clean old notification tokens (older than 180 days)
async function cleanNotifications() {
    const result = await pgQuery(`DELETE FROM notifications WHERE created_at < (TIMESTAMP '${new Date().toISOString()}' - INTERVAL '180 days');`);

    console.log(`${result.rowCount} tokens have been deleted`);

    return;
}

exports.handler = async (event, context, callback) => {
    // psql client pool
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    await Promise.all([
        cleanMessages(),
        cleanEvents(),
        /*cleanNotifications()*/
    ]);

    // end psql
    global.pgPool.end();
};