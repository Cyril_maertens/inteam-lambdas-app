const axios = require('axios');

exports.handler = async (event) => {
    console.log("ping start");
    const params = {
        url: process.env.PONG_URL,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'x-api-key': 'EuXuSfhBep2G6omvMsfhUao4FE6WjuOZ1AalZbHa'
        },
    };
    
    await axios(params).then((data) => {
        console.log("ping call pong success", data);
    }).catch((err) => {
        console.log("ping call pong error", err);
    });
    
};