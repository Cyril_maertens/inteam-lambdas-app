'use strict';

const xlsx = require('node-xlsx');
const AWS = require('aws-sdk');
const csv = require('papaparse');
const noaccent = require('atonic');
const moment = require('moment-timezone');
const pg = require('pg');


function transform(string, index) {
    return string.trim().replace(/'/g, `''`);
}

function handleUndef(val) {
    if (!val) {
        return null;
    } else {
        return `${val}`.replace(/[\r\n]/g,'').replace(/,/g,'-');
    }
}

function formatName(name) {
    return name.toUpperCase();
}

function formatFirstname(name) {
    const strArray = name.split('-');
    let str = '';
    for (let i in strArray) {
        str += `${strArray[i].slice(0, 1).toUpperCase()}${strArray[i].slice(1, strArray[i].length).toLowerCase()}-`;
    }
    return str.substring(0, str.length - 1);
}

function formatField(field) {
    const str = `${field.slice(0, 1).toUpperCase()}${field.slice(1, field.length).toLowerCase()}`;
    return noaccent(str);
}

function formatRole(field) {
    if (field === 'collab') {
        return 'collaborateur';
    }
    if (field === 'rh') {
        return 'rh';
    }
    if (field === 'com') {
        return 'commercial';
    }
    if (field === 'admin') {
        return 'administration';
    }
}

function handleNullVal(val) {
    if (!val || val === 'null') {
        return null;
    } else {
        return `'${val}'`;
    }
}

function verifyField(value, regex) {
    if (!value || value === 'null') {
        return true;
    }

    const compare = value.match(regex);
    if (!compare || compare[0] != value) {
        return false;
    } else {
        return true;
    }
}

function recursiveVerify(index, collab, clients, sites) {
    if (index == - 1) {
        return false;
    }
    if (formatField(collab.site) != sites[index] || formatField(collab.client) != clients[index]) {
        const idx = clients.indexOf(formatField(collab.client), index + 1);
        return recursiveVerify(idx, collab, clients, sites);
    }
    else {
        return true;
    }
}

async function getExtension(fileName) {
    const fileNameArray = fileName.split('.');
    return fileNameArray[fileNameArray.length - 1];
}

function getObject(bucket, key) {
    return new Promise((resolve, reject) => {
        const s3 = new AWS.S3();

        s3.getObject({
            Bucket: bucket,
            Key: key
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    });
}

function putObject(bucket, key, content) {
    return new Promise((resolve, reject) => {
        const s3 = new AWS.S3();

        s3.putObject({
            Bucket: bucket,
            Key: key,
            Body: content
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    });
}

// get uploaded excel
function getExcel(content) {
    const read = xlsx.parse(content);
    return read;
}

// parse excel to json
function xlsxToJson(sheet) {

    let string = '';
    for (let i in sheet) {
        for (let j in sheet[0]) {
            string += `${handleUndef(sheet[i][j])},`;
        }
        string = `${string.substring(0, string.length - 1)}
`;
    }

    const bufferString = string.split('\n');
    bufferString[0] = noaccent(bufferString[0].toLowerCase());

    let stringToParse = '';
    for (let i in bufferString) {
        stringToParse += `${bufferString[i]}
`;
    }

    const parsed = csv.parse(stringToParse, { header: true, trimHeader: true, transform: transform, skipEmptyLines: true });

    return parsed;
}


//error handling
function errorHandling(parsed, validField, requiredField, verifyRegex, name) {

    if (parsed.errors[0]) {
        for (let i in parsed.errors) {
            console.error(`Bad request: ${parsed.errors[i].message} at line ${parsed.errors[i].row + 1} in sheet ${name}`);
        }
    }

    for (let i in validField) {
        if (parsed.meta.fields.indexOf(validField[i]) == -1) {
            console.error(`Bad request: the required column ${validField[i]} is missing in sheet ${name}. Hint: columns names are not case or accent sensitive`);
        }
    }

    for (let i in parsed.data) {
        for (let j in requiredField) {
            if (!parsed.data[i][requiredField[j]]) {
                console.error(`Bad request: required field ${requiredField[j]} is missing line ${Number(i) + 1} in sheet ${name}`);
            }
        }
    }

    for (let i in parsed.data) {
        for (let j in validField) {
            if (!verifyField(parsed.data[i][validField[j]], verifyRegex[j])) {
                console.error(`Bad request: field ${validField[j]} is malformatted line ${Number(i) + 1}`);
            }
        }
    }

    return;
}

function verifyDataConsistency(collab, site) {
    let clients = [], sites = [];

    for (let i in site) {
        clients.push(formatField(site[i].client));
        sites.push(formatField(site[i].site));
    }

    for (let i in collab) {
        const index = clients.indexOf(formatField(collab[i].client));

        if (index == -1) {
            console.error(`Bad request: client doesn't exists in sheet Consultants at line ${Number(i) + 1}`);
        } else {
            if (!recursiveVerify(index, collab[i], clients, sites)) {
                console.error(`Bad request: client & site missmatch in sheet Consultants at line ${Number(i) + 1}`);
            }
        }
    }

    return;
}

function queryResult(res) {
    if (res == 0) {
        return '; A value have not been inserted';
    }
    if (res > 1) {
        return '; Too many value may have been inserted';
    }
    return '; All Good';
}

async function insertInDb(collab, site, excel) {

    const pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    let query = '';

    for (let i in collab) {
        if (collab[i].u_id != 'null') {
            console.log(`Nothing to do: user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} already in database`);
            continue;
        }

        const user = await pgPool.query(`SELECT id FROM users WHERE lastname='${formatName(collab[i].nom)}' AND firstname='${formatFirstname(collab[i].prenom)}';`);

        if (!user.rows[0]) {
            query = `INSERT INTO users (lastname,firstname,email,phone,description,birthdate,role,picture) SELECT CAST('${formatName(collab[i].nom)}' AS VARCHAR),CAST('${formatFirstname(collab[i].prenom)}' AS VARCHAR),CAST(${handleNullVal(collab[i].email)} AS VARCHAR),CAST(${handleNullVal(collab[i].telephone)} AS VARCHAR),CAST(${handleNullVal(collab[i].presentation)} AS VARCHAR),CAST(${handleNullVal(collab[i].naissance)} AS DATE),roles.id,'default_profile_picture.png' FROM roles WHERE roles.name='${formatRole(collab[i].role)}' RETURNING id;`;
            console.log(query);

            await pgPool.query(query).then((res) => {
                console.log(`Success: ${res.command} user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} ${queryResult(res.rowCount)}`);
                excel[0].data[Number(i) + 1][excel[0].data[0].indexOf('u_id')] = res.rows[0].id;
            }).catch((err) => {
                console.error(err);
            });
        } else {
            console.log(`Nothing to do: user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} already in database`);
            excel[0].data[Number(i) + 1][excel[0].data[0].indexOf('u_id')] = user.rows[0].id;
        }
    }

    for (let i in site) {
        if (site[i].s_id != 'null') {
            console.log(`Nothing to do: site ${formatField(site[i].client)} ${formatField(site[i].site)} already in database`);
            continue;
        }

        const client = await pgPool.query(`SELECT id FROM clients WHERE name='${formatField(site[i].client)}';`);

        if (!client.rows[0]) {
            query = `INSERT INTO clients (name,logo) VALUES (CAST('${formatField(site[i].client)}' AS VARCHAR),'default_client_logo.png') RETURNING id;`;
            console.log(query);

            await pgPool.query(query).then((res) => {
                console.log(`Success: ${res.command} client ${formatField(site[i].client)} ${queryResult(res.rowCount)}`);
                excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('c_id')] = res.rows[0].id;
            }).catch((err) => {
                console.error(err);
            });
        } else {
            console.log(`Nothing to do: client ${formatField(site[i].client)} already in database`);
            excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('c_id')] = client.rows[0].id;
        }

        if (handleNullVal(site[i].nom) && handleNullVal(site[i].prenom)) {
            query = `INSERT INTO sites (name,num,street,code,city,address_2,phone,client,sales_man) SELECT CAST('${formatField(site[i].site)}' AS VARCHAR),CAST(${await handleNullVal(site[i].numero)} AS INT),CAST(${await handleNullVal(site[i].rue)} AS VARCHAR),CAST(${await handleNullVal(site[i].code)} AS VARCHAR),CAST('${site[i].ville}' AS VARCHAR),CAST(${await handleNullVal(site[i].complement)} AS VARCHAR),CAST(${await handleNullVal(site[i].telephone)} AS VARCHAR),clients.id,users.id from clients,users WHERE clients.name='${formatField(site[i].client)}' AND users.familyname='${formatName(site[i].nom)}' AND users.firstname='${formatFirstname(site[i].prenom)}' RETURNING sites.id;`;
            console.log(query);

            await pgPool.query(query).then((res) => {
                console.log(`Success: ${res.command} site ${formatField(site[i].client)} ${formatField(site[i].site)} ${queryResult(res.rowCount)}`);
                excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('s_id')] = res.rows[0].id;
                excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('added_date')] = moment.tz('Europe/Paris').format('YYYY-MM-DD HH:mm');
            }).catch((err) => {
                console.error(err);
            });

        } else {
            query = `INSERT INTO sites (name,num,street,code,city,address_2,phone,client) SELECT CAST('${formatField(site[i].site)}' AS VARCHAR),CAST(${await handleNullVal(site[i].numero)} AS INT),CAST(${await handleNullVal(site[i].rue)} AS VARCHAR),CAST(${await handleNullVal(site[i].code)} AS VARCHAR),CAST('${site[i].ville}' AS VARCHAR),CAST(${await handleNullVal(site[i].complement)} AS VARCHAR),CAST(${await handleNullVal(site[i].telephone)} AS VARCHAR),clients.id from clients WHERE clients.name='${formatField(site[i].client)}' RETURNING sites.id;`;
            console.log(query);

            await pgPool.query(query).then((res) => {
                console.log(`Success: ${res.command} site ${formatField(site[i].client)} ${formatField(site[i].site)} ${queryResult(res.rowCount)}`);
                excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('s_id')] = res.rows[0].id;
                excel[1].data[Number(i) + 1][excel[1].data[0].indexOf('added_date')] = moment.tz('Europe/Paris').format('YYYY-MM-DD HH:mm');
            }).catch((err) => {
                console.error(err);
            });
        }
    }

    for (let i in collab) {
        if (collab[i].added_date != 'null') {
            if (collab[i].updated_date != 'null' || collab[i].fin === 'null') {
                console.log(`Nothing to do: user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} on mission ${formatField(collab[i].mission)} on site ${formatField(collab[i].client)} ${formatField(collab[i].site)} already up to date`);
                continue;
            } else {
                const m_id = await pgPool.query(`SELECT missions.id FROM missions,sites,clients WHERE missions.name='${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)}' AND missions.site=sites.id AND sites.client=clients.id AND sites.name='${formatField(collab[i].site)}' AND clients.name='${formatField(collab[i].client)}';`);
                await pgPool.query(`UPDATE missions SET end_date=CAST('${collab[i].fin}' AS DATE) WHERE "user"=CAST(${collab[i].u_id} AS INT) AND id=${m_id.rows[0].id}';`).then((res) => {
                    console.log(`Success: ${res.command} user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} on mission ${formatField(collab[i].mission)} on site ${formatField(collab[i].client)} ${formatField(collab[i].site)} ${queryResult(res.rowCount)}`);
                    excel[0].data[Number(i) + 1][excel[0].data[0].indexOf('updated_date')] = moment.tz('Europe/Paris').format('YYYY-MM-DD HH:mm');
                }).catch((err) => {
                    console.error(err);
                });
                continue;
            }
            console.log(`Nothing to do: user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} on mission ${formatField(collab[i].mission)} on project ${formatField(collab[i].projet)} on site ${formatField(collab[i].client)} ${formatField(collab[i].site)} already up to date`);
            continue;
        }
        query = `INSERT INTO missions (name,start_date,end_date,site,"user") SELECT CAST('${formatField(collab[i].mission)}' AS VARCHAR),CAST(${await handleNullVal(collab[i].debut)} AS DATE),CAST(${await handleNullVal(collab[i].fin)} AS DATE),sites.id,users.id FROM clients,sites,users WHERE clients.name='${formatField(collab[i].client)}' AND sites.name='${formatField(collab[i].site)}' AND sites.client=clients.id AND users.lastname=CAST('${formatName(collab[i].nom)}' AS VARCHAR) AND users.firstname=CAST('${formatFirstname(collab[i].prenom)}' AS VARCHAR);`;
        console.log(query);

        await pgPool.query(query).then((res) => {
            console.log(`Success: ${res.command} user ${formatName(collab[i].nom)} ${formatFirstname(collab[i].prenom)} on mission ${formatField(collab[i].mission)} on site ${formatField(collab[i].client)} ${formatField(collab[i].site)}${queryResult(res.rowCount)}`);
            excel[0].data[Number(i) + 1][excel[0].data[0].indexOf('added_date')] = moment.tz('Europe/Paris').format('YYYY-MM-DD HH:mm');
        }).catch((err) => {
            console.error(err);
        });
    }

    pgPool.end();

    return;
}

const collabFields = ['nom', 'prenom', 'email', 'telephone', 'presentation', 'naissance', 'role', 'client', 'site', 'mission', 'debut', 'fin'];
const collabRequiredFields = ['nom', 'prenom', 'role', 'client', 'site', 'mission'];
const collabRegExp = [
    RegExp(`([^\\d ]+[ ]?)+`, 'g'), //nom
    RegExp(`([^\\d ]+[ ]?)+`, 'g'), //prenom
    RegExp(`.+\..+@.+\..+`, 'g'), //email
    RegExp('.*', 'g'), //telephone
    RegExp('.*', 'g'), //presentation
    RegExp(`[0-9]{4}[-\/][0-9]{2}[-\/][0-9]{2}`, 'g'), //naissance
    RegExp(`collab|rh|com|admin`, 'g'), //role
    RegExp('.*', 'g'), //client
    RegExp('.*', 'g'), //site
    RegExp('.*', 'g'), //mission
    RegExp(`[0-9]{4}[-\/][0-9]{2}[-\/][0-9]{2}`, 'g'), //debut
    RegExp(`[0-9]{4}[-\/][0-9]{2}[-\/][0-9]{2}`, 'g'), //fin
];

const siteFields = ['client', 'site', 'numero', 'rue', 'code', 'ville', 'complement', 'telephone', 'nom', 'prenom'];
const siteRequiredFields = ['client', 'site', 'ville'];
const siteRegExp = [
    RegExp('.*', 'g'), //client
    RegExp('.*', 'g'), //site
    RegExp(`[0-9]+[ ]?(bis)?`, 'g'), //numero
    RegExp('.*', 'g'), //rue
    RegExp(`[0-9]{2}[ ]?[0-9]{3}`, 'g'), //code
    RegExp('.*', 'g'), //ville
    RegExp('.*', 'g'), //complement
    RegExp('.*', 'g'), //telephone
    RegExp(`([^\\d ]+[ ]?)+`, 'g'), //nom
    RegExp(`([^\\d ]+[ ]?)+`, 'g'), //prenom
];

async function main(event) {
    const object = await getObject(process.env.BUCKET_NAME, process.env.OBJECT_KEY);

    const excel = await getExcel(object.Body);

    const jsonArray = await Promise.all([
        xlsxToJson(excel[0].data),
        xlsxToJson(excel[1].data)
    ]);

    await Promise.all([
        errorHandling(jsonArray[0], collabFields, collabRequiredFields, collabRegExp, excel[0].name),
        errorHandling(jsonArray[1], siteFields, siteRequiredFields, siteRegExp, excel[1].name)
    ]);

    await verifyDataConsistency(jsonArray[0].data, jsonArray[1].data);

    await insertInDb(jsonArray[0].data, jsonArray[1].data, excel).catch((err) => {
        console.error(err);
        return Promise.reject(err);
    });

    await putObject(process.env.BUCKET_NAME, 'imported', xlsx.build(excel));

    return;
}


exports.handler = async (event, context, callback) => {
    await main(event).then(() => {
        console.log('end, no error');
    }).catch((err) => {
        console.error(err);
    });
};