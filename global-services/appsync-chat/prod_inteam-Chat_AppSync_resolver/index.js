/*

AppSync resolver

runtime : nodejs8.10

env vars : 
    - CONNECTION_STRING
    - NOTIFICATION_TOPIC_ARN

psql user : appsyncresolver

IAM role : Lambda_AppSync_resolver

IAM authorizations : 
    - Cloudwatch logs
    - VPC operations
    - SNS publish

VPC config : standard

switch attached_API_endpoints : 
    - getAllConversations
    - getConversation
    - newConversation
    - newMessage

*/

'use strict';

const pg = require('pg');
const AWS = require('aws-sdk');


// function to query db
async function pgQuery(query) {
    const result = await global.pgPool.query(query);

    return result;
}

// function to get user to send notification to      // useless in v2
function getToUser(fromUser, user1, user2) {
    if (fromUser == user1) {
        return user2;
    } else {
        return user1;
    }
}

// function to puclish to sns and invoke lambda notification pusher
function publish(payload) {
    const sns = new AWS.SNS();

    return new Promise((resolve, reject) => {
        sns.publish({
            Message: payload,
            TopicArn: process.env.NOTIFICATION_TOPIC_ARN
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// function returning all conversations and their last message
async function getAllConversations(userId) { //, identity

    // verify user's indentity from token, not usable with api key
    /*if(userId != identity.claims['custom:id']) {
        return;
    }*/

    // get conversations of a given user
    const result = await pgQuery(`select * from users_get_conversations(${userId});`);

    // format response according to graphql schema
    let endResult = [];
    for (let i in result.rows) {
        /*
        await Promise.all([
            pgQuery(`select * from get_messages(${result.rows[i].id}) limit 1;`),
            getUserInfos(userId, result.rows[i].user1, result.rows[i].user2)
        ])
        */
        // get last message of each conversation
        const tempResult1 = await pgQuery(`select * from conversation_get_messages(${result.rows[i].id}) limit 1;`);
        // get users participating to each conversation (not optimized for v1, mandatory for v2)
        const tempResult2 = await pgQuery(`select id from conversation_get_users(${result.rows[i].id});`);
        // get info of users participating (needs to be changed in v2)
        const tempResult3 = await getUserInfos(userId, tempResult2.rows[0].id, tempResult2.rows[1].id);
        endResult.push({
            "id": result.rows[i].id,
            "user": {
                "id": tempResult3.rows[0].id,
                "familyname": tempResult3.rows[0].lastname,
                "firstname": tempResult3.rows[0].firstname,
                "picture": tempResult3.rows[0].picture
            },
            "page": {
                "nextToken": 1,
                "messages": [{
                    "id": tempResult1.rows[0].id,
                    "from_user": tempResult1.rows[0].user,
                    "to_user": await getToUser(tempResult1.rows[0].user, tempResult3.rows[0].id, userId),
                    "emit_date": tempResult1.rows[0].date,
                    "content": tempResult1.rows[0].content,
                    "is_sent": tempResult1.rows[0].is_sent,
                    "is_seen": 0,
                    "owned": tempResult1.rows[0].conversation
                }]
            }
        });
    }
    return endResult;
}

// return user's info (not used in v2)
async function getUserInfos(id, id1, id2) {
    if (id == id1) {
        return await pgQuery(`select id,lastname,firstname,picture from users_get_byid(${id2});`);
    }
    else {
        return await pgQuery(`select id,lastname,firstname,picture from users_get_byid(${id1});`);
    }
}

// return next token for messages' paging
async function nextToken(conversationId, token) {
    // query messages count in a conversation
    const count = await pgQuery(`select count(id) from conversation_get_messages(${conversationId});`);
    // return calculated token
    if (count.rows[0].count <= (Number(token) + global.pageSize)) {
        return null;
    } else {
        return (Number(token) + global.pageSize);
    }
}

// function to format message according to graphql schema
async function formatMessages(messages, user1, user2) {
    let endResult = [];
    for (let i in messages) {
        endResult.push({
            "id": messages[i].id,
            "from_user": messages[i].user,
            "to_user": await getToUser(messages[i].user, user1, user2),
            "emit_date": messages[i].date,
            "content": messages[i].content,
            "is_sent": messages[i].is_sent,
            "is_seen": 0,
            "owned": messages[i].conversation
        });
    }

    return endResult;
}

// function returning a conversation and its messages paged
async function getConversation(me, user, token) { //, identity

    // verify user's indentity from token, not usable with api key
    /*if(userId != identity.claims['custom:id']) {
        return;
    }*/

    // get conversation's id according to the 2 users participating (useless in v2)
    const conversationId = await pgQuery(`SELECT * FROM get_conversation_id(${me},${user});`);

    // check 404
    if (!conversationId.rows[0]) {
        return null;
    } else {

        // get conversation's info
        const conversation = await pgQuery(`SELECT * FROM get_conversation(${conversationId.rows[0].id});`);
        /*
        await Promise.all([
            pgQuery(`select * from get_messages(${conversation.rows[0].id}) limit 10 offset ${Number(token)};`),
            pgQuery(`select id,familyname,firstname,picture from users_get_byid(${user});`)
        ])
        */

        // get conversation's messages paged
        const messages = await pgQuery(`select * from conversation_get_messages(${conversation.rows[0].id}) limit ${global.pageSize} offset ${Number(token)};`);
        // get user's info (will changed in v2)
        const userInfo = await pgQuery(`select id,lastname,firstname,picture from users_get_byid(${user});`);
        // format response according to graphql schema
        const endResult = {
            "id": conversation.rows[0].id,
            "user": {
                "id": userInfo.rows[0].id,
                "familyname": userInfo.rows[0].lastname,
                "firstname": userInfo.rows[0].firstname,
                "picture": userInfo.rows[0].picture
            },
            "page": {
                "nextToken": await nextToken(conversation.rows[0].id, token),
                "messages": await formatMessages(messages.rows, me ,user)
            }
        };
        return endResult;
    }
}
/* not used
async function getLastMessage(conversationId) {
    const result = await pgQuery(`select * from conversation_get_last_message(${conversationId});`);
    return result.rows[0];
}*/

// function to insert char ' in db
function handleSingleQuote(string) {
    return string.replace(/'/g, `''`);
}

async function parseTokens(invitedUsers) {
    let result = []/*, string = ''*/;
    /*for (let i in invitedUsers) {
        string += `${invitedUsers[i].id},`;
    }
    string = string.substring(0, string.length - 1);*/

    const tokens = await pgQuery(`SELECT token FROM notifications WHERE "user" IN (${invitedUsers});`);

    for (let i in tokens.rows) {
        result.push(tokens.rows[i].token);
    }

    return result;
}


// function to create a conversation and returns it
async function newConversation(from_user, to_user, emit_date, content) { //, identity

    // verify user's indentity from token, not usable with api key
    /*if(from_user != identity.claims['custom:id']) {
        return;
    }*/

    // insert new conversation in db
    const result = await pgQuery(`select * from new_conversation('${from_user}+${to_user}','${new Date().toISOString()}');`);

    const promiseResult = await Promise.all([
        // insert conversation's first message in db
        pgQuery(`select * from new_message(${from_user},'${new Date().toISOString()}','${content}',${result.rows[0].id});`),
        // get message writer info
        pgQuery(`SELECT lastname,firstname FROM users_get_byid(${from_user});`),
        // link new conversation to the 2 users participating
        pgQuery(`INSERT INTO user_conversation ("user",conversation) VALUES (${from_user},${result.rows[0].id});`),
        pgQuery(`INSERT INTO user_conversation ("user",conversation) VALUES (${to_user},${result.rows[0].id});`),
    ]);

    // format data to pusblish to sns
    const payload = JSON.stringify({
        "type": "pushMessage",
        "messageData": {
            "id": promiseResult[0].rows[0].id,
            "content": promiseResult[0].rows[0].content,
            "emit_date": promiseResult[0].rows[0].date,
            "sender_familyname": promiseResult[1].rows[0].lastname,
            "sender_firstname": promiseResult[1].rows[0].firstname,
            "from_user": from_user,
            "to_user": to_user,
            "is_sent": 1,
            "is_seen": 0,
            "owned": promiseResult[0].rows[0].conversation
        },
        "toUser": await parseTokens(to_user)
    });

    // pusblish to sns to invoke lambda notification pusher
    await publish(payload).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    // return new conversation
    return await getConversation(from_user, to_user, 0);
}

// function to create a new message in a conversation and returns it
async function newMessage(args) { //, identity

    // verify user's indentity from token, not usable with api key
    /*if(args.from_user != identity.claims['custom:id']) {
        return;
    }*/

    const promiseResult = await Promise.all([
        // insert message in db
        pgQuery(`select * from new_message(${args.from_user},'${new Date().toISOString()}','${await handleSingleQuote(args.content)}',${args.owned});`),
        // get other user's info
        pgQuery(`SELECT lastname,firstname FROM users_get_byid(${args.from_user});`)
    ]);

    // format data to publish to sns
    const payload = JSON.stringify({
        "type": "pushMessage",
        "messageData": {
            "id": promiseResult[0].rows[0].id,
            "content": promiseResult[0].rows[0].content,
            "emit_date": promiseResult[0].rows[0].date,
            "sender_familyname": promiseResult[1].rows[0].lastname,
            "sender_firstname": promiseResult[1].rows[0].firstname,
            "from_user": args.from_user,
            "to_user": args.to_user,
            "is_sent": 1,
            "is_seen": 0,
            "owned": promiseResult[0].rows[0].conversation
        },
        "toUser": await parseTokens(args.to_user)
    });

    // pusblish to sns to invoke lambda notification pusher
    await publish(payload).catch((err) => {
        console.error(err);
        return Promise.reject({
            callbackStatusCode: 500,
            callbackBody: JSON.stringify('Internal server error')
        });
    });

    // return the new message
    return {
        "id": promiseResult[0].rows[0].id,
        "from_user": args.from_user,
        "to_user": args.to_user,
        "emit_date": promiseResult[0].rows[0].date,
        "content": promiseResult[0].rows[0].content,
        "is_sent": promiseResult[0].rows[0].is_sent,
        "is_seen": 0,
        "owned": promiseResult[0].rows[0].conversation
    };
}

global.pageSize = 50;

exports.handler = async (event, context, callback) => {
    // pg client pool
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    // switch between cases defined in graphql schema
    switch (event.field) {

        case 'getAllConversations':
            console.log('getAllConversations');
            callback(null, await getAllConversations(event.arguments.me)); //, event.identity
            break;

        case 'getConversation':
            console.log('getConversation');
            callback(null, await getConversation(event.arguments.me, event.arguments.user, event.arguments.token)); //, event.identity
            break;

        case 'newConversation':
            console.log('newConversation');
            callback(null, await newConversation(event.arguments.from_user, event.arguments.to_user, event.arguments.emit_date, event.arguments.content)); //, event.identity
            break;

        case 'newMessage':
            console.log('newMessage');
            callback(null, await newMessage(event.arguments)); //, event.identity
            break;

        default:
            callback(null, '400');
            break;
    }

    // pg client end
    global.pgPool.end();
};


/*  **** V2 *****
'use strict';

const pg = require('pg');
const AWS = require('aws-sdk');

// function to query DB
async function pgQuery(query) {
    result = await global.pgPool.query(query);
    return result;
}

// function handling char ' in DB
async function handleSingleQuote(string) {
    return string.replace(/'/g, `''`);
}

// resolve next token for conversation's paging
async function nextToken(conversationId, token) {
    const count = await pgQuery(`SELECT COUNT(id) FROM conversation_get_messages(${conversationId});`);
    if (count.rows[0].count <= ((Number(token) + 1) * 10)) {
        return null;
    } else {
        return (Number(token) + 1);
    }
}

// resolve if a conversation between several users already exists or not
// return conversation's id if it exists, else return false
async function resolveIfAlreadyExists(from_user, users) {
    const conversationsToCompare = await pgQuery(`SELECT id FROM users_get_conversations(${from_user});`);

    let usersConversations = [], tempResult;

    for (let i in users) {
        let tempArray = [];
        const tempResult = await pgQuery(`SELECT id FROM users_get_conversations(${users[i]});`);
        tempResult.rows.forEach((currentValue) => {
            tempArray.push(currentValue.id);
        });
        usersConversations.push(tempArray);
    }

    for (let i in conversationsToCompare.rows) {
        if (usersConversations.every((currentValue) => {
            if (currentValue.indexOf(conversationsToCompare.rows[i].id) === -1) {
                return false;
            } else {
                return true;
            }
        })) {
            return conversationsToCompare.rows[i].id;
        }
    }

    return false;
}

// create a conversation's name based on users' name
async function handleConversationName(me, users) {
    let string = '';
    for (let i in users) {
        string += `${users[i]},`;
    }
    const usersName = await pgQuery(`SELECT firstname FROM users WHERE id IN (${string}${me});`);

    string = '';
    for (let i in usersName.rows) {
        string += `${usersName.rows[i].firstname},`;
    }
    return string.substring(0, string.length - 1);
}

// return all conversation (of a user) with their last message
async function getAllConversations(meId) {
    // get conversations of a user
    const conversations = await pgQuery(`SELECT * FROM users_get_conversations(${meId});`);

    let endResult = [];
    for (let i in conversations.rows) {
        const tempResult = await Promise.all([
            // get conversations' users
            pgQuery(`SELECT * FROM conversation_get_users(${conversations.rows[i].id}) WHERE id!=${meId};`),
            // get conversations' last message
            pgQuery(`SELECT * FROM conversation_get_last_message(${conversations.rows[i].id});`)
        ])
        // format response based on graphql schema
        endResult.push({
            "id": conversations.rows[i].id,
            "created_at": conversations.rows[i].created_at,
            "name": conversations.rows[i].name,
            "users": tempResult[0].rows,
            "page": {
                "nextToken": 0,
                "messages": tempResult[0].rows[0]
            }
        });
    }

    return endResult;
}

// return a conversation and its messages (paged)
async function getConversation(me, conversationId, token) {

    const conversation = await Promise.all([
        // get a conversation
        pgQuery(`SELECT * FROM get_conversation(${conversationId});`),
        // update flag seen_at
        pgQuery(`UPDATE user_conversation SET seen_at=CAST('${new Date().toISOString()}' AS TIMESTAMP) WHERE user=${me};`)
    ]);

    const promisesResult = await Promise.all([
        // resolve next token for paging
        nextToken(conversationId, token),
        // get conversation's users
        pgQuery(`SELECT * FROM conversation_get_users(${conversations.rows[i].id});`),
        // get conversation's messages (paged)
        pgQuery(`SELECT * FROM conversation_get_messages(${conversations.rows[i].id}) limit 10 offset ${Number(token) * 10};`)
    ]);
    // format response based on graphql schema
    const response = {
        "id": conversation[0].rows[0].id,
        "create_date": conversation[0].rows[0].created_at,
        "name": conversation[0].rows[0].name,
        "users": promisesResult[1].rows,
        "page": {
            "nextToken": promisesResult[0],
            "messages": promisesResult[2].rows
        }
    };

    return response;
}

// link users to conversation in DB in case a conversation is created
async function linkUserToConversation(users, conversationId) {
    for (let i in users) {
        await pgQuery(`INSERT INTO user_conversation(user,conversation) VALUES (${users[i].id},${conversationId});`);
    }
}

// create a new conversation
async function newConversation(from_user, users, content) {
    //check if conversation with specified users already exists
    const conversationId = await resolveIfAlreadyExists(from_user, users);

    if (conversationId) { // if already exists
        // insert message in it
        await newMessage(from_user, content, conversationId);
        // return updated conversation
        return await getConversation(from_user, conversationId, 0);
    } else { // if doesn't exists
        // create new conversation
        const newConversation = await pgQuery(`SELECT * FROM new_conversation('${await handleConversationName(from_user, users)}','${new Date().toISOString()}');`);
        // link users to it
        await linkUserToConversation(users, newConversation.rows[0].id);
        // insert message in it
        await newMessage(from_user, content, newConversation.rows[0].id);
        // return the new conversation
        return await getConversation(from_user, newConversation.rows[0].id, 0);
    }
}

// insert a new message in a conversation
async function newMessage(from_user, content, conversation) {
    // insert message in DB
    const newMessage = await pgQuery(`SELECT * FROM new_message(${from_user},'${new Date().toISOString()}','${await handleSingleQuote(content)}',${conversation});`);
    // invoke lambda notifications publisher
    const lambdaParams = {
        FunctionName: process.env.PUBLISHER_NAME,
        InvocationType: 'Event',
        LogType: 'Tail',
        Payload: `{"type":"pushMessage","id":"${newMessage.rows[0].id}"}`
    };

    const request = lambda.invoke(lambdaParams);
    request.send();
    // return the new message
    return newMessage.rows[0];
}

// lambda client
const lambda = new AWS.Lambda();

exports.handler = async (event, context, callback) => {
    global.pgPool = new pg.Pool({
        connectionString: process.env.CONNECTION_STRING,
        ssl: true,
        max: 10,
        min: 3,
        idleTimeoutMillis: 10000,
        connectionTimeoutMillis: 1000
    });

    switch (event.field) {

        case 'getAllConversations':
            console.log('getAllConversations');
            callback(null, await getAllConversations(event.arguments.me));
            break;

        case 'getConversation':
            console.log('getConversation');
            callback(null, await getConversation(event.arguments.me, event.arguments.conversation, event.arguments.token));
            break;

        case 'newConversation':
            console.log('newConversation');
            callback(null, await newConversation(event.arguments.me, event.arguments.users, event.arguments.content));
            break;

        case 'newMessage':
            console.log('newMessage');
            callback(null, await newMessage(event.arguments.me, event.arguments.content, event.arguments.conversation));
            break;

        default:
            callback(null, '400');
            break;
    }

    global.pgPool.end();
}; */