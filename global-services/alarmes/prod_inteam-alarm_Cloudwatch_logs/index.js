/*

Triggered by Cloudwatch alarms - logs it

runtime : nodejs8.10

IAM role : Lambda_Basic_Execution

IAM authorizations : 
    - Cloudwatch logs
    - SES:sendMail

VPC config : none

ENV vars :
    - SOURCE_EMAIL
    - DEST_EMAIL
    - SLACK_URL

attached_SNS_topic : inteam-alarm_topic

*/

'use strict';

const AWS = require('aws-sdk');
const axios = require('axios');

// function to send email to admins
function sendMail(params) {
    return new Promise((resolve, reject) => {
        const ses = new AWS.SES();

        ses.sendEmail({
            Destination: {
                ToAddresses: [
                    params.toAddresse
                ]
            },
            Message: {
                Body: {
                    Text: {
                        Data: params.content
                    }
                },
                Subject: {
                    Data: params.subject
                }
            },
            Source: process.env.SOURCE_EMAIL
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

function pushToSlack(subject){
    const payload = {
        "icon_emoji": ":hammer_and_wrench:",
        "username": `CloudWatch alert ${new Date().toLocaleString()}`,
        "text": `${subject}`
    };

    const params = {
        url: process.env.SLACK_URL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: payload
    };

    axios(params).then((data) => {
        console.log(data);
    }).catch((err) => {
        console.error(err);
    });
}


exports.handler = async (event, context, callback) => {
    // logs the alarm
    console.log(event.Records[0].Sns);

    // send mail to amdin
    await sendMail({
        "toAddresse": process.env.DEST_EMAIL,
        "content": event.Records[0].Sns.Message,
        "subject": event.Records[0].Sns.Subject
    }).then((data) => {
        console.log('email successfully sent');
    }).catch((err) => {
        console.error('error sendind email : ' + err);
    });

    await pushToSlack(event.Records[0].Sns.Subject);

    return;
};